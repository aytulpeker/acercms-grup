﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;
using AcerCMS.ModelService;

namespace AcerCMS.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            var model = NewsModelService.GetIndexViewModel();
            return View(model);
        }

        public ActionResult ReadNews(int id)
        {
            var model = NewsModelService.ReadNews(id);
            
            if(model== null)
                return HttpNotFound("Haber Bulunamadı!");

            return View(model);           
        }

    }
}