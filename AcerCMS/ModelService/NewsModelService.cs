﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.ModelService
{
    public class NewsModelService
    {
        public static List<News> GetIndexViewModel()
        {           
            using (var db = new AcerCmsEntities())
            {
                return db.News.OrderByDescending(o => o.UpdateDate).ToList();
            }         
        }

        public static NewsViewModel ReadNews(int id)
        {
            using (var db = new AcerCmsEntities())
            {
                return new NewsViewModel()
                {
                    News = db.News.Find(id)                                      
                };
            }
        }
    }
}